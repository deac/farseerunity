/*
* FarseerUnity based on Farseer Physics Engine port:
* Copyright (c) 2012 Gabriel Ochsenhofer https://github.com/gabstv/Farseer-Unity3D
* 
* Original source Box2D:
* Copyright (c) 2011 Ian Qvist http://farseerphysics.codeplex.com/
* 
* This software is provided 'as-is', without any express or implied 
* warranty.  In no event will the authors be held liable for any damages 
* arising from the use of this software. 
* Permission is granted to anyone to use this software for any purpose, 
* including commercial applications, and to alter it and redistribute it 
* freely, subject to the following restrictions: 
* 1. The origin of this software must not be misrepresented; you must not 
* claim that you wrote the original software. If you use this software 
* in a product, an acknowledgment in the product documentation would be 
* appreciated but is not required. 
* 2. Altered source versions must be plainly marked as such, and must not be 
* misrepresented as being the original software. 
* 3. This notice may not be removed or altered from any source distribution. 
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FSTransform = FarseerPhysics.Common.Transform;
using Transform = UnityEngine.Transform;
using Microsoft.Xna.Framework;

using Category = FarseerPhysics.Dynamics.Category;

[AddComponentMenu("FarseerUnity/Collision/Circle Shape Component")]
public class FSCircleShapeComponent : FSShapeComponent
{
	public float radius = 1.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnDrawGizmos()
	{
		SphereCollider sc = GetComponent<SphereCollider>();
		//if(sc != null)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(transform.position, radius*transform.lossyScale.x);
		}
	}
	
	public override Shape GetShape()
	{
		/*SphereCollider sc = GetComponent<SphereCollider>();
		if(sc == null)
			return null;*/
		return new CircleShape(radius*transform.lossyScale.x, Density);
	}
}
